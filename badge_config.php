<?php

static $badgeArrayUser = array(

    '1' => array(

        'NAME' => 'Developer',
        'DESC' => '',

    ),

    '2' => array(

        'NAME' => 'Administrator',
        'DESC' => '',

    ),

    '3' => array(

        'NAME' => 'Moderator',
        'DESC' => '',

    ),

    '4' => array(

        'NAME' => 'Translator',
        'DESC' => '',

    ),

    '5' => array(

        'NAME' => 'Premium',
        'DESC' => '',

    ),

    '6' => array(

        'NAME' => 'Beta tester',
        'DESC' => '',

    ),

    '7' => array(

        'NAME' => 'Best player',
        'DESC' => 'First ranked at the end of the round',

    ),

    '8' => array(

        'NAME' => 'Runner up',
        'DESC' => 'Second ranked at the end of the round',

    ),

    '9' => array(

        'NAME' => '3rd placed',
        'DESC' => '3rd placed at the end of the round',

    ),

    '10' => array(

        'NAME' => 'Almost there',
        'DESC' => 'Ranked among 10 top players of the round',

    ),

    '11' => array(

        'NAME' => 'Doom!',
        'DESC' => 'Launched a doom attack',

    ),

    '12' => array(

        'NAME' => 'ANTI-Doom!',
        'DESC' => 'Disabled a doom attack',

    ),

    '13' => array(

        'NAME' => 'DDoS!',
        'DESC' => 'Launched a DDoS attack',

    ),


);


static $badgeArrayClan = array(

    '1' => array(

        'NAME' => 'Best clan',
        'DESC' => 'Winner',

    ),

    '2' => array(

        'NAME' => 'Runner up',
        'DESC' => 'Almost winner',

    ),

    '3' => array(

        'NAME' => '3rd placed',
        'DESC' => 'Close..',

    ),

);

?>
