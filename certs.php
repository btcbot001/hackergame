<?php

static $certs = array(

    '1' => array(

        'NAME' => 'Basic tutorial',
        'DESC' => 'Follow this tutorial to learn how to play',
        'PRICE' => '0',

    ),

    '2' => array(

        'NAME' => 'Hacking 101',
        'DESC' => 'Learn the basics of hacking',
        'PRICE' => '0',

    ),

    '3' => array(

        'NAME' => 'Intermediate hacking',
        'DESC' => 'Improve your hacking techniques',
        'PRICE' => '50',

    ),

    '4' => array(

        'NAME' => 'Advanced hacking',
        'DESC' => 'Meet the wonders of DDoS world',
        'PRICE' => '200',

    ),

    '5' => array(

        'NAME' => 'DDoS security',
        'DESC' => 'Learn to protect yourself from DDoS',
        'PRICE' => '500',

    )

);


?>
