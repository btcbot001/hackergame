<?php

// 2019: I don't remember/think this is used...

//storyline
$securityCompany = 'Massive Dynamic';
$securityCompanyIP = '16843010';
$securityCompanyID = '58';

$storylineCompany = 'NSA';
$storylineCompanyIP = '16843009';
$storylineCompanyID = '57';

static $exploitInfo = array(

    'NAME' => 'necromancer',
    'VERSION' => '10',
    'SIZE' => '10000',
    'RAM' => '2048',
    'TYPE' => '50',

);

static $empLauncherInfo = array(

    'NAME' => 'EMP Launcher',
    'VERSION' => '10',
    'SIZE' => '10000',
    'RAM' => '2048',
    'TYPE' => '51',

);

$launchDuration = '21600'; //6 horas

static $corpInfo = array(

    '1' => array(

        'NAME' => 'RDR corp',
        'IP' => '16843011',
        'CEO' => 'Fê Lemos',
        'REGIONS' => '1/6',

    ),


    '2' => array(

        'NAME' => 'NdN inc',
        'IP' => '16843012',
        'CEO' => 'Toddy Mattos',
        'REGIONS' => '2/8',

    ),

    '3' => array(

        'NAME' => 'YaoMing company',
        'IP' => '16843013',
        'CEO' => 'Renatusso',
        'REGIONS' => '4/5',

    ),

    '4' => array(

        'NAME' => 'Fear 2.0',
        'IP' => '16843014',
        'CEO' => 'Dinossaur',
        'REGIONS' => '3/7',

    ),

);

static $continentInfo = array(

    '1' => 'America',
    '2' => 'Europe',
    '3' => 'Asia',

);

$storylineMaxLevel = '60';

?>
