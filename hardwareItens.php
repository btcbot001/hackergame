<?php

static $cpuItens = array(

    '1' => array(
        'NAME' => 'i286',
        'DESC' => 'i286 bla bla',
        'POW' => '500',
        'PRICE' => '99'
    ),

    '2' => array(
        'NAME' => 'P2',
        'DESC' => 'i7 irra',
        'POW' => '1000',
        'PRICE' => '99'
    ),

    '3' => array(
        'NAME' => 'P3',
        'DESC' => 'P3',
        'POW' => '1500',
        'PRICE' => '150'
    ),

    '4' => array(
        'NAME' => 'P4',
        'DESC' => 'P4',
        'POW' => '2000',
        'PRICE' => '300'
    ),

    '5' => array(
        'NAME' => 'Dual Core',
        'DESC' => 'i7 irra',
        'POW' => '2500',
        'PRICE' => '600'
    ),

    '6' => array(
        'NAME' => 'Quad Core',
        'DESC' => 'i7 irra',
        'POW' => '3000',
        'PRICE' => '1500'
    ),

    '7' => array(
        'NAME' => 'AMD FX 8350',
        'DESC' => 'i7 irra',
        'POW' => '3500',
        'PRICE' => '2000'
    ),

    '8' => array(
        'NAME' => 'i7 4990k',
        'DESC' => 'i7 irra',
        'POW' => '4000',
        'PRICE' => '5000'
    ),

);

static $ramItens = array(

    '1' => array(
        'NAME' => 'Alzheimer',
        'DESC' => 'Better than nothing...',
        'POW' => '256',
        'PRICE' => '100'
    ),

    '2' => array(
        'NAME' => 'King Ton',
        'DESC' => '...',
        'POW' => '512',
        'PRICE' => '99'
    ),

    '3' => array(
        'NAME' => 'Cross 1G',
        'DESC' => '...',
        'POW' => '1024',
        'PRICE' => '500'
    ),

    '4' => array(
        'NAME' => 'Elephpant',
        'DESC' => '...',
        'POW' => '2048',
        'PRICE' => '2500'
    ),

);

static $hddItens = array(

    '1' => array(
        'NAME' => 'miniSSD 100',
        'DESC' => 'Old...',
        'POW' => '100',
        'PRICE' => '99'
    ),

    '2' => array(
        'NAME' => 'miniSSD 500',
        'DESC' => 'Old...',
        'POW' => '500',
        'PRICE' => '99'
    ),

    '3' => array(
        'NAME' => 'SSD1',
        'DESC' => '...',
        'POW' => '1000',
        'PRICE' => '300'
    ),

    '4' => array(
        'NAME' => 'SSD2',
        'DESC' => '...',
        'POW' => '2000',
        'PRICE' => '1000'
    ),

    '5' => array(
        'NAME' => 'SSD5',
        'DESC' => '...',
        'POW' => '5000',
        'PRICE' => '3000'
    ),

    '6' => array(
        'NAME' => 'SSD10',
        'DESC' => '...',
        'POW' => '10000',
        'PRICE' => '8000'
    ),

);

static $netItens = array(

    '1' => array(
        'NAME' => 'NET1',
        'DESC' => 'Better than nothing...',
        'POW' => '1',
        'PRICE' => '100'
    ),

    '2' => array(
        'NAME' => 'NET2',
        'DESC' => '...',
        'POW' => '2',
        'PRICE' => '50'
    ),

    '3' => array(
        'NAME' => 'NET4',
        'DESC' => '...',
        'POW' => '4',
        'PRICE' => '99'
    ),

    '4' => array(
        'NAME' => 'MEGA10',
        'DESC' => '...',
        'POW' => '10',
        'PRICE' => '250'
    ),

    '5' => array(
        'NAME' => 'MEGA25',
        'DESC' => '...',
        'POW' => '25',
        'PRICE' => '1000'
    ),

    '6' => array(
        'NAME' => 'MEGA50',
        'DESC' => '...',
        'POW' => '50',
        'PRICE' => '2500'
    ),

    '7' => array(
        'NAME' => 'BROAD100',
        'DESC' => '...',
        'POW' => '100',
        'PRICE' => '10000'
    ),

    '8' => array(
        'NAME' => 'BROAD250',
        'DESC' => '...',
        'POW' => '250',
        'PRICE' => '25000'
    ),

    '9' => array(
        'NAME' => 'ULTRA500',
        'DESC' => '...',
        'POW' => '500',
        'PRICE' => '50000'
    ),

    '10' => array(
        'NAME' => 'ULTRA1000',
        'DESC' => '...',
        'POW' => '1000',
        'PRICE' => '100000'
    ),

);

static $xhdItens = array(

    '0' => array(

        'NAME' => '',
        'DESC' => '',
        'POW' => '',
        'PRICE' => '500'

    ),

    '1' => array(
        'NAME' => 'Basic X',
        'DESC' => '...',
        'POW' => '200',
        'PRICE' => '50'
    ),

    '2' => array(
        'NAME' => 'Inter X',
        'DESC' => '...',
        'POW' => '500',
        'PRICE' => '99'
    ),

    '3' => array(
        'NAME' => 'Mega X',
        'DESC' => '...',
        'POW' => '1000',
        'PRICE' => '500'
    ),

);

?>
